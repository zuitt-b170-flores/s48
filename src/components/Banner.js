/*
import the following:
	React from react

	Jumbotron from react-bootstrap/Jumbotron
	Button from react-bootstrap/Button
	Row from react-bootstrap/Row
	Col from react-bootstrap/Col
*/
// Dependencies
import React from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner(){
	return(
		<Row>
			<Col>
				<Container fluid>
	{/*				<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for Everyone, Everywhere.</p>
					<Button variant="primary">Enroll Now!</Button>*/}
					<h1>Page Not Found</h1>
					<p>Go back to <a href="/">Home Page</a></p>
				</Container>
			</Col>
		</Row>
	)
}