// Base Imports
import React, { useState } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom'; - Switch is depricated that's why we need to use Routes function inside react-router-dom instead
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// App Components
/*
  every component has to be imported before it can be rendered inside the index.js
*/
import AppNavbar from './components/AppNavbar.js';

// Page Components
// import Home from './pages/Home.js';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error'


export default function App() {

	const [ user, setUser ] = useState(null);

	return (
	   <Router>
	    <AppNavbar user={user} />
	    <Routes>
	      <Route path = "/" element={<Error />} /> 
	      <Route path = "/courses" element={<Courses />} /> 
	      <Route path = "/register" element={<Register/>} /> 
	      <Route path = "/login" element={<Login />} /> 
	    </Routes> 
	   </Router>
	)
}